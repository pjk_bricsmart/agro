#
# This is a Shiny web application. You can run the application by clicking
# the 'Run App' button above.
#


library(shiny)
library(tidyverse)
library(readxl)
library(magrittr)
library(here)
library(combinat)
library(DT)

# Define UI for application that draws a histogram
ui <- fluidPage(
    # Application title
    titlePanel("Solvent Blend Distance Calculator"),
    
    # Sidebar with a slider input for number of bins
    sidebarLayout(
        sidebarPanel(
            textInput("ref_name",
                      "Reference solvent name",
                      value = "Agrotain"),
            numericInput("ref_dD",
                         "Reference solvent dD",
                         value = 17.5),
            numericInput("ref_dP",
                         "Reference solvent dP",
                         value = 11.0),
            numericInput("ref_dH",
                         "Reference solvent dH",
                         value = 12.4),
            numericInput(
                "step",
                "Step size",
                value = 0.05,
                min = 0.01,
                max = 0.25
            ),
            fileInput("solvents",
                      "Select solvents file",
                      placeholder = "No file selected"),
            actionButton("submit",
                         "Calculate combinatoric distance table")
        ),
        
        # Show a plot of the generated distribution
        mainPanel(
            tableOutput("reference_table"),
            tableOutput("step_size"),
            dataTableOutput("solvents_table"),
            dataTableOutput("results_table")
        )
    )
)

# Define server logic required to draw a histogram
server <- function(input, output) {
    pct_1 <- c(seq(0, 1.0, 0.1))
    pct_2 <- c(seq(0, 1.0, 0.1))
    pct_3 <- c(seq(0, 1.0, 0.1))
    prs <- data.frame(
        pct1 = numeric(),
        pct2 = numeric(),
        pct3 = numeric(),
        stringsAsFactors = FALSE
    )
    n <- 1
    for (i in pct_1) {
        for (j in pct_2) {
            for (k in pct_3) {
                qaz <- i + j + k
                if (qaz == 1.0) {
                    prs[n, 'pct1'] <- i
                    prs[n, 'pct2'] <- j
                    prs[n, 'pct3'] <- k
                    n <- n + 1
                }
            }
        }
    }
    
    output$reference_table <- renderTable({
        refTable <- data.frame(
            Solvent = character(1),
            dD = numeric(1),
            dP = numeric(1),
            dH = numeric(1)
        )
        refTable$Solvent <- input$ref_name
        refTable$dD <- input$ref_dD
        refTable$dP <- input$ref_dP
        refTable$dH <- input$ref_dH
        
        refTable
    })
    
    output$step_size <- renderTable({
        stepTable <- data.frame(Step_Size = numeric(1))
        stepTable$Step_Size <- input$step
        stepTable
    })
    
    df_solvents_upload <- reactive({
        inFile <- input$solvents
        if (is.null(inFile))
            return(NULL)
        df <- readxl::read_xlsx(inFile$datapath)
        return(df)
    })
    
    output$solvents_table <- DT::renderDataTable({
        df <- df_solvents_upload()
        print(head(df))
        DT::datatable(df)
    })
    
    output$results_table <- DT::renderDataTable({
        pct_1 <- c(seq(0, 1.0, 0.1))
        pct_2 <- c(seq(0, 1.0, 0.1))
        pct_3 <- c(seq(0, 1.0, 0.1))
        prs <- data.frame(
            pct1 = numeric(),
            pct2 = numeric(),
            pct3 = numeric(),
            stringsAsFactors = FALSE
        )
        n <- 1
        for (i in pct_1) {
            for (j in pct_2) {
                for (k in pct_3) {
                    qaz <- i + j + k
                    if (qaz == 1.0) {
                        prs[n, 'pct1'] <- i
                        prs[n, 'pct2'] <- j
                        prs[n, 'pct3'] <- k
                        n <- n + 1
                    }
                }
            }
        }
        
        df <- df_solvents_upload()
        
        print(' in ')
        
        print(head(df))
        
        df['index'] <- 1:nrow(df)
        
        results <- data.frame(
            Solvent_1 = character(),
            Solvent_1_pct = numeric(),
            Solvent_2 = character(),
            Solvent_2_pct = numeric(),
            Solvent_3 = character(),
            Solvent_3_pct = numeric(),
            distance = numeric(),
            stringsAsFactors = FALSE
        )
        
        S <- c(1:nrow(df))
        r <- 3
        combination <- t(combn(S, r))
        # combination
        
        n <- 1
        for (m in 1:nrow(combination)) {
            i <- combination[m, 1]
            j <- combination[m, 2]
            k <- combination[m, 3]
            ###
            for (PCT in 1:nrow(prs)) {
                # print(paste0(n, ' ', i, ' ', j, ' ', k))
                # calculate pair distance
                dDm = df[i, 'dD'] * prs[PCT, 1] + df[j, 'dD'] * prs[PCT, 2] + df[k, 'dD'] * prs[PCT, 3]
                dPm = df[i, 'dP'] * prs[PCT, 1] + df[j, 'dP'] * prs[PCT, 2] + df[k, 'dP'] * prs[PCT, 3]
                dHm = df[i, 'dH'] * prs[PCT, 1] + df[j, 'dH'] * prs[PCT, 2] + df[k, 'dP'] * prs[PCT, 3]
                dist <-
                    sqrt(
                        4 * (dDm - input$ref_dD) ** 2 + (dPm - input$ref_dP) ** 2 + (dHm - input$ref_dH) ** 2
                    )
                #
                # print(paste0('One = ', df[i, 'Solvent'], ' Two = ', df[j, 'Solvent'], ' Distance = ', dist))
                results[n, 'Solvent_1'] <- df[i, 'Solvent']
                results[n, 'Solvent_1_pct'] <- prs[PCT, 1]
                results[n, 'Solvent_2'] <- df[j, 'Solvent']
                results[n, 'Solvent_2_pct'] <- prs[PCT, 2]
                results[n, 'Solvent_3'] <- df[k, 'Solvent']
                results[n, 'Solvent_3_pct'] <- prs[PCT, 3]
                results[n, 'distance'] <- dist
                #
                ###
                n <- n + 1
            }
        }
        
        print(head(results))
        
        DT::datatable(results)
    })
}

# Run the application
shinyApp(ui = ui, server = server)
